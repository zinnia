#pragma once

#include <string>
#include <memory>
#include <experimental/filesystem>
#include <cpptoml.h>
#include "project.h"

namespace fs = std::experimental::filesystem;

struct Config {
	/*static Config find(std::string filename) {
		fs::path searchdir = ".";

		return Config::from_file(path);
	}*/
	static Config from_file(fs::path path) {
		auto manifest = cpptoml::parse_file(path);
		return Config::from_toml(manifest);
	}
	static Config from_toml(std::shared_ptr<cpptoml::table> manifest) {
		auto config = Config();
		config.stylepath = manifest->get_as<std::string>("style").value_or("");

		return config;
	}

	std::string stylepath;
};

struct DemoState {
	DemoState(std::shared_ptr<Config> config)
		: running(true), current_project(nullptr),
		  config(config) {}

    int width, height; float ratio;

    std::shared_ptr<Project> current_project;
    std::shared_ptr<Config> config;

	bool running;
};
