#include "geom.h"

std::ostream &operator<<(std::ostream &os, const ImVec2 &vec) {
  os << "[" << vec.x << ", " << vec.y << "]";
  return os;
}

std::ostream &operator<<(std::ostream &os, const Rect &rect) {
  os << "Rect { pos: " << rect.pos << ", size: " << rect.size << " }";
  return os;
}
