#include <map>
#include <cerrno>
#include <Inotify.h>
#include <FileSystemEvent.h>
#include <experimental/filesystem>
#include "project.h"

namespace fs = std::experimental::filesystem;

#define MAX_WDS 16

class NotifyService {
public:
	NotifyService() : inotify{IN_MODIFY | IN_MOVED_TO} {}

	void watch(std::shared_ptr<Project> project) {
		inotify.watchDirectoryRecursively(project->basepath);
		projects[project->basepath]= project;
	}

	void _poll() {
		auto event = inotify.nb_getNextEvent();
		if (event) {
			if (event->recursive_root_path) {
				auto path = *event->recursive_root_path;
				if (projects.find(path) != projects.end()) {
					std::cout << "=== reloading shaders ===\n";
					projects[path]->reload_shaders();
				}
			}
		}
	}
private:
	Inotify inotify;

	std::map<fs::path, std::shared_ptr<Project>> projects;
};
