* enhanced sphere tracing (mercury paper)
* better materials
    - ferris's dull material raymarching method ([logicoma - engage breakdown](https://www.youtube.com/watch?v=woqksTHNbvk))
    - emissive materials
    - reflections
* node-based shaders and buffers
    - Model/View based architecture
        + Model
        + node view
        + render combined view+controller
* inputs
    - Audio analysis
        + Beat detection
        + FFTs
