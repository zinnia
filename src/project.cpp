#include "project.h"
#include <iostream>

Project Project::from_file(fs::path path) {
	auto manifest = cpptoml::parse_file(path);
	auto basepath = fs::absolute(path).parent_path();

	auto project = Project::from_toml(manifest, basepath);
	return project;
}

Project Project::from_toml(std::shared_ptr<cpptoml::table> manifest,
		fs::path basepath = fs::path(".")) {
	auto project = Project();
	project.basepath = basepath;
	project.name = manifest->get_qualified_as<std::string>("project.name").value_or("");
	project.shader_bundle = std::move(std::make_shared<ShaderBundle>());

	auto sceneconf = manifest->get_table_array("scene");
	if (sceneconf) {
		for (const auto& conf_scene : *sceneconf) {
			auto scene = std::make_shared<Scene>();
			scene->name = conf_scene->get_as<std::string>("name").value_or("");
			auto p_frag = conf_scene->get_as<std::string>("frag");
			auto p_vert = conf_scene->get_as<std::string>("vert");

			if (p_frag && p_vert) {
				std::vector<std::pair<fs::path, ShaderBundle::ShaderType>> shaders;
				shaders.push_back(std::make_pair(basepath / *p_vert, GL_VERTEX_SHADER));
				shaders.push_back(std::make_pair(basepath / *p_frag, GL_FRAGMENT_SHADER));
				scene->shader_p = project.shader_bundle->add_program(shaders);
			}

			project.scenes.push_back(std::move(scene));
		}
	}

	auto boot_scene = manifest->get_qualified_as<int>("project.boot_scene");
	if (boot_scene) {
		printf("loading boot scene %i\n", *boot_scene);
		project.load_scene(project.scenes[*boot_scene]);

		project.shader_bundle->recompile().link();
	}

	return project;
}

void Project::load_scene(std::shared_ptr<Scene> scene) {
	this->current_scene = scene;
	this->scene_loaded = true;
}

void Project::reload_shaders() {
	this->shader_bundle->recompile().link();
}
