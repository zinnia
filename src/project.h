#pragma once

#include <string>
#include <vector>
#include <memory>
#include <experimental/filesystem>
#include <cpptoml.h>

#include "shaders.h"

namespace fs = std::experimental::filesystem;

class Scene {
public:
	std::string name;

	ShaderBundle::ProgramHandle* shader_p;
};

class NullScene : public Scene {
};

class Project {
public:
	Project() = default;

	static Project from_file(fs::path path);
	static Project from_toml(std::shared_ptr<cpptoml::table> manifest, fs::path basepath);

	void load_scene(std::shared_ptr<Scene> scene);
	void reload_shaders();

	void unload_project();

	std::vector<std::shared_ptr<Scene>> scenes;
	std::shared_ptr<Scene> current_scene;
	bool scene_loaded = false;
	std::string name;
	fs::path basepath;

    std::shared_ptr<ShaderBundle> shader_bundle;
};
