#pragma once

#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui.h>
#include <imgui_internal.h>
#include <sstream>

class Rect {
public:
    Rect(ImVec2 pos, ImVec2 size) : pos(pos), size(size) {}
    Rect(float x, float y, float w, float h) : pos(x, y), size(w, h) {}

    ImVec2 pos;
    ImVec2 size;

    float top() const    { return pos.y; }
    float left() const   { return pos.x; }
    float bottom() const { return pos.y + size.y; }
    float right() const  { return pos.x + size.x; }

    bool intersects(const Rect &other) {
        return this->left() < other.right() &&
               this->right() > other.left() &&
               this->top() < other.bottom() &&
               this->bottom() > other.top();
    }
};

std::ostream &operator<<(std::ostream &os, const ImVec2 &vec);
std::ostream &operator<<(std::ostream &os, const Rect &rect);
