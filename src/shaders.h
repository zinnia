#pragma once

#include <GL/gl3w.h>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

class ShaderBundle {
public:
    using ShaderHandle = GLuint;
    using ProgramHandle = GLuint;
    using ShaderType = GLenum;

    struct ShaderId {
		fs::path path;
        ShaderType type;
        bool operator<(const ShaderId& rhs) const {
            return std::tie(path, type) < std::tie(rhs.path, rhs.type);
        }
        bool operator==(const ShaderId& rhs) const {
            return std::tie(path, type) == std::tie(rhs.path, rhs.type);
        }
    };

    struct Shader {
        ShaderHandle handle;

        int32_t hash;
    };

    struct Program {
        // public handle; 0 until the program has successfully linked
        ProgramHandle handle;

        ProgramHandle internal_handle;
    };

    ShaderBundle() = default;
    ~ShaderBundle();

    std::map<ShaderId, Shader> shader_pool;
    std::map<std::vector<ShaderId>, Program> programs;

    ProgramHandle* add_program(const std::vector<std::pair<fs::path, ShaderType>>& shaders);

    ShaderBundle& recompile();
    ShaderBundle& link();

    static std::string preprocess(fs::path path, std::string shader);
};
