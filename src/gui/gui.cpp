#include <vector>
#include <iostream>
#include <assert.h>
#include <imgui.h>
#include "gui.h"
#include "../project.h"


void Gui::render() {
    if (present_mode)
        return;

	if (imgui_demo_open)
        ImGui::ShowTestWindow(&imgui_demo_open);

    if (project_overlay_open)
        this->render_project_overlay();

    if (node_editor_open)
        this->node_editor.draw_window("Node Editor", &node_editor_open);

    this->render_menubar();
}

void Gui::render_project_overlay() {
	ImGui::SetNextWindowPos(ImVec2(20, 30));
	if (ImGui::Begin("", &this->project_overlay_open, ImVec2(200,0), 0.3f, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings)) {
		ImGui::Text("[project]");
		ImGui::Separator();
		if (state->current_project != nullptr) {
			ImGui::Text("name: %s", state->current_project->name.c_str());
			ImGui::Text("n_scenes: %d", static_cast<int>(state->current_project->scenes.size()));
			if (!state->current_project->scene_loaded) {
				ImGui::Text("no scene loaded");
			} else {
				ImGui::Text("current scene: %s",
					state->current_project->current_scene->name.c_str());
			}
		} else {
			ImGui::Text("no project loaded.");
		}
		ImGui::End();
	}
}

void Gui::render_menubar() {
	if (ImGui::BeginMainMenuBar()) {
		if (ImGui::BeginMenu("File"))
			render_mmb_file();

		if (ImGui::BeginMenu("View"))
			render_mmb_view();

		if (ImGui::BeginMenu("Debug"))
			render_mmb_debug();

		ImGui::EndMainMenuBar();
	}
}

void Gui::render_mmb_file() {
	if (ImGui::MenuItem("Load project", "CTRL+O")) {
		/*nfdchar_t *out_path = nullptr;
		nfdresult_t result = NFD_OpenDialog(NULL, NULL, &out_path);
		if (result == NFD_OKAY) {
			// Load project
			auto new_project = Project::from_file(std::string(out_path));
			state->current_project = std::move(new_project);

			free(out_path);
		} else if (result == NFD_ERROR) {
			std::cout << "Error in filepicker: " << NFD_GetError() << '\n';
		}*/
	}

	ImGui::Separator();
	if (ImGui::MenuItem("Exit", "CTRL+Q"))
		state->running = false;

	ImGui::EndMenu();
}

void Gui::render_mmb_view() {
	ImGui::MenuItem("Node Editor", NULL, &this->node_editor_open);
	ImGui::MenuItem("Meta overlay", NULL, &this->project_overlay_open);

	ImGui::Separator();

	if (ImGui::MenuItem("Reload ImGui styling"))
		this->load_style(cpptoml::parse_file(state->config->stylepath));

	ImGui::MenuItem("Show ImGui demo window", NULL, &this->imgui_demo_open);

	ImGui::EndMenu();
}

void Gui::render_mmb_debug() {
	ImGui::MenuItem("Profiler");

	if (ImGui::MenuItem("Reload shaders", "CTRL+R")) {
		if (state->current_project != nullptr) {
			if (state->current_project->scene_loaded) {
				std::cout << "=== reloading shaders ===\n";
				state->current_project->reload_shaders();
			}
		}
	}

	ImGui::EndMenu();
}


static cpptoml::option<ImVec2> vec2im2(cpptoml::option<std::vector<double>> vec) {
	if (vec) {
		assert((*vec).size() == 2);
		return ImVec2((*vec)[0], (*vec)[1]);
	} else {
		return cpptoml::option<ImVec2>();
	}
}

static cpptoml::option<ImVec4> vec2im4(cpptoml::option<std::vector<double>> vec) {
	if (vec) {
		assert((*vec).size() == 4);
		return ImVec4((*vec)[0], (*vec)[1], (*vec)[2], (*vec)[3]);
	} else {
		return cpptoml::option<ImVec4>();
	}
}

#define st_lds_d(n, k) style.n = conf->get_qualified_as<double>(k).value_or(defaultstyle.n)
#define st_lds_v2(n, k) style.n = vec2im2(conf->get_qualified_array_of<double>(k)).value_or(defaultstyle.n)
#define st_lds_v4(n, k) style.n = vec2im4(conf->get_qualified_array_of<double>(k)).value_or(defaultstyle.n)


void Gui::load_style(std::shared_ptr<cpptoml::table> conf) {
	ImGuiStyle &style = ImGui::GetStyle();
	auto defaultstyle = ImGuiStyle();

	st_lds_d(Alpha, "imgui.alpha");
	st_lds_v2(WindowPadding, "imgui.window.padding");
	st_lds_v2(WindowMinSize, "imgui.window.min_size");
	st_lds_d(WindowRounding, "imgui.window.rounding");
	st_lds_v2(WindowTitleAlign, "imgui.window.title_align");
	st_lds_d(ChildWindowRounding, "imgui.window.child_rounding");
	st_lds_v2(FramePadding, "imgui.frame.padding");
	st_lds_d(FrameRounding, "imgui.frame.rounding");
}
