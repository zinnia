#pragma once

#include <memory>
#include <cpptoml.h>
#include "../state.h"
#include "node_editor.h"

class Gui {
public:
	Gui(DemoState *state) : state(state), node_editor{}
	{
		this->load_style(cpptoml::parse_file(state->config->stylepath));
	}

	void render();

	bool perf_open = false;
	bool project_overlay_open = true;
	bool imgui_demo_open = false;
	bool node_editor_open = true;

    /// Skip rendering the GUI, for presenting stuff.
    bool present_mode = false;

    NodeEditor node_editor;

private:
	DemoState *state;

	void render_project_overlay();

	void render_menubar();
	void render_mmb_file();
	void render_mmb_view();
	void render_mmb_debug();

	void load_style(std::shared_ptr<cpptoml::table> style);
};
