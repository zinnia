struct SceneResult {
	float d;
	float mat_idx;
};

SceneResult min_sr(SceneResult a, SceneResult b) {
	return (a.d < b.d) ? a : b;
}
